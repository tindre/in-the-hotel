//
//  JTRoomViewCell.swift
//  In the Hotel
//
//  Created by Jakub Tomanik on 26/12/2014.
//  Copyright (c) 2014 Jakub Tomanik. All rights reserved.
//

import UIKit



class JTRoomViewCell: UITableViewCell {
  
  @IBOutlet weak var roomNumberLabel: UILabel!
  @IBOutlet weak var bedsLabel: UILabel!
  @IBOutlet weak var priceLabel: UILabel!
  @IBOutlet weak var availableLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
  
  func formatCell(room: JTRoom) {
    if self.priceLabel != nil {
      self.priceLabel.text = "\(room.price) EUR"
    }
    if self.bedsLabel != nil {
      let formater = NSNumberFormatter()
      formater.numberStyle = NSNumberFormatterStyle.SpellOutStyle
      formater.locale = NSLocale(localeIdentifier: "en")
      self.bedsLabel.text = formater.stringFromNumber(room.beds)
    }
    if self.availableLabel != nil {
      self.availableLabel.text = "no"
      if room.available { self.availableLabel.text = "yes" }
    }
    if self.roomNumberLabel != nil {
      self.roomNumberLabel.text = "\(room.roomNumber)"
    }
  }

}
