//
//  JTTableViewHeader.swift
//  In the Hotel
//
//  Created by Jakub Tomanik on 28/12/2014.
//  Copyright (c) 2014 Jakub Tomanik. All rights reserved.
//

import UIKit

class JTBookingViewGuestHeader: UITableViewHeaderFooterView {
  @IBOutlet weak var captionLabel: UILabel!
}

class JTBookingViewGuestFooter: UITableViewHeaderFooterView {
  @IBOutlet weak var childButton: UIButton!
  @IBOutlet weak var adultButton: UIButton!
}

class JTBookingViewRoomHeader: UITableViewHeaderFooterView {
  @IBOutlet weak var leftLabel: UILabel!
  @IBOutlet weak var middleLabel: UILabel!
  @IBOutlet weak var rightLabel: UILabel!
}

class JTBookingViewRoomFooter: UITableViewHeaderFooterView {
  @IBOutlet weak var mergeButton: UIButton!
}