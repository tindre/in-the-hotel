//
//  JTRoomsViewController.swift
//  In the Hotel
//
//  Created by Jakub Tomanik on 26/12/2014.
//  Copyright (c) 2014 Jakub Tomanik. All rights reserved.
//

import UIKit
import Alamofire

class JTRoomsViewController: UITableViewController {
  /** reference to main hotel object */
  var table:[JTRoom] = []
  
  override func viewDidLoad() {
    //println("viewDidLoad")
    
    super.viewDidLoad()
    
    self.pullDataFromRemote()
    
    var refreshingControl = UIRefreshControl()
    refreshingControl.addTarget(self, action: Selector("pullDataFromRemote"), forControlEvents: UIControlEvents.ValueChanged)
    self.refreshControl = refreshingControl

  }
  
  override func viewWillAppear(animated: Bool) {
    //println("viewWillAppear")
  }
  
  func pullDataFromRemote() {
    println("pullDataFromRemote")
    self.table.removeAll(keepCapacity: false)
    Alamofire.request(.GET, "http://nodejs-inthehotel.rhcloud.com/rooms/").responseJSON { (req, res, json, error) in
      if (error != nil) {
        println("error: \(res)")
      } else {
        let data = JSON(json!)
        for (index:String, value:JSON) in data {
          let newRoom:JTRoom = JTRoom()
          newRoom.parseFromJSON(value)
          self.table.append(newRoom)
        }
        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
      }
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - Table view data source
  
  override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    // Return the number of sections.
    return 1
  }
  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    // Return the number of rows in the section.
    //println(self.table.count)
    return self.table.count
  }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("JTRoomViewCell", forIndexPath: indexPath) as JTRoomViewCell
    return cell
  }
  
  override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    if let currentCell = cell as? JTRoomViewCell {
      currentCell.formatCell(self.table[indexPath.row])
    }
  }
  
  
  /*
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
  // Get the new view controller using [segue destinationViewController].
  // Pass the selected object to the new view controller.
  }
  */
  
}
