//
//  puzzleSolution.swift
//  In the Hotel
//
//  Created by Jakub Tomanik on 02/01/2015.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//


extension JTHotel {
  /**
  Finds rooms that can be booked for certain guests.
  
  :param: An array with guests' data.
  :returns: Optional, returns an array with rooms that can be booked, or nil if guests cannot be accommodated.
  */
  func findRoomsForGuests(guests:[JTGuest]) -> [JTRoom]? {
    //println("findRoomsForGuests")
    
    /** Optional variable to be returned */
    var rooms:[JTRoom]?
    
    /** Number of adults among guests */
    var adults = guests.countWhere {$0.adult}
    
    /** Number of children among guests */
    var children = guests.count - adults
    
    /** Indicates failure to accommodate guests */
    var failedToBook = false
    
    // Fail, if there are not enough beds available to accommodate the guests.
    if guests.count > self.availableBeds { return rooms }
    
    // Fail, if it is impossible to accommodate all of the children, even in the biggest rooms.
    if children > 2*adults { return rooms }
    
    /**
    Local function, to find a room for one adult.
    
    :returns: Room instance, if a room can be found; nil otherwise.
    */
    func adultOnly() -> JTRoom?{
      /** Note: Although it is stated that the hotel owner wants to earn as much as possible per booking
      common sense dictates that we should try to accommodate single adult in a single room before we'll try to put him/her in a double/triple room.
      
      Whether we should try to put two or three adults in a double/triple room if there is such a combination in a booking, it remains an open question. Since we want to maximise the profit, I don't optimise to put as much people in a room as possible.
      */
      var reserved =  self.getAvailableRoom(1)
      if reserved == nil { reserved =  self.getAvailableRoom(2) }
      if reserved == nil { reserved =  self.getAvailableRoom(3) }
      if reserved != nil { adults -= 1 }
      return reserved
    }
    
    /**
    Local function, to find a room for one adult and one child.
    
    :returns: Room instance, if a room can be found; nil otherwise.
    */
    func adultAndChild()  -> JTRoom?{
      /** Note: Although it is stated that the hotel owner wants to earn as much as possible per booking
      common sense dictates that we should try to accommodate adult with child in a double room before we try to put them in a triple room.
      */
      var reserved =  self.getAvailableRoom(2)
      if reserved == nil { reserved =  self.getAvailableRoom(3) }
      if reserved != nil { adults -= 1; children -= 1 }
      return reserved
    }
    
    /**
    Local function, to find a room for one adult and two children
    
    :returns: Room instance, if a room can be found; nil otherwise.
    */
    func adultAndTwoChildren()  -> JTRoom?{
      let reserved = self.getAvailableRoom(3)
      if  reserved != nil { adults -= 1; children -= 2 }
      return reserved
    }
    
    rooms = Array<JTRoom>()
    while adults > 0 && failedToBook == false {
      switch (adults, children) {
      case let (a, c) where a > 0 && c == 0:
        // adults only
        if let room = adultOnly() {
          room.available = false
          rooms!.append(room)
        } else {
          failedToBook = true
        }
      case let (a, c) where c < a :
        /** There are less children than adults in a group.
        First, we need  to find accommodation for the children.
        Since we want to maximise the profit, we don`t try to
        put our guests in the big rooms first.
        */
        if let room = adultAndChild() {
          room.available = false
          rooms!.append(room)
        } else {
          failedToBook = true
        }
      case let (a, c) where c == a :
        /** There are as many adults as children. To accommodate them, we need to put them in pairs. */
        if let room = adultAndChild() {
          room.available = false
          rooms!.append(room)
        } else {
          failedToBook = true
        }
      case let (a, c) where c > a :
        /** There are more children than adults. We need to accommodate children in the triple rooms first. */
        if let room = adultAndTwoChildren() {
          room.available = false
          rooms!.append(room)
        } else {
          failedToBook = true
        }
      default:
        break
      }
    }
    
    // We had to temporarily alter availibity of the rooms, so now we need to undo this.
    rooms!.each { $0.available = true }
    
    if failedToBook { return nil }
    return rooms
  }
}