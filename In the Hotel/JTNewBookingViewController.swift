//
//  JTBookingViewController.swift
//  In the Hotel
//
//  Created by Jakub Tomanik on 26/12/2014.
//  Copyright (c) 2014 Jakub Tomanik. All rights reserved.
//

import UIKit
import Alamofire

class JTNewBookingViewController: UITableViewController {
  /** reference to main hotel object */
  var hotel:JTHotel = JTHotel()
  
  /** currently displayed booking */
  var booking = JTBooking()
  
  /** when true editing is not possible */
  var displayOnlyMode = false
  
  /** height of footer/header cells in the table */
  var headerHeight:CGFloat = 48
  
  /** Book button on navigation bar */
  @IBOutlet weak var bookButton: UIBarButtonItem!
  
  override func viewDidLoad() {
    //println("viewDidLoad")
    
    super.viewDidLoad()
    
    // load headers and footers
    self.tableView.sectionHeaderHeight = self.headerHeight
    
    let roomHeaderNib = UINib(nibName: "JTBookingViewRoomHeader", bundle: nil)
    let guestHeaderNib = UINib(nibName: "JTBookingViewGuestHeader", bundle: nil)
    
    self.tableView.registerNib(roomHeaderNib, forHeaderFooterViewReuseIdentifier: "JTBookingViewRoomHeader")
    self.tableView.registerNib(guestHeaderNib, forHeaderFooterViewReuseIdentifier: "JTBookingViewGuestHeader")
    
    if !self.displayOnlyMode {
      let guestFooterNib = UINib(nibName: "JTBookingViewGuestFooter", bundle: nil)
      //let roomFooterNib = UINib(nibName: "JTBookingViewRoomFooter", bundle: nil)
      
      self.tableView.registerNib(guestFooterNib, forHeaderFooterViewReuseIdentifier: "JTBookingViewGuestFooter")
      //self.tableView.registerNib(roomFooterNib, forHeaderFooterViewReuseIdentifier: "JTBookingViewRoomFooter")
    }
  }
  
  override func viewWillAppear(animated: Bool) {
    //println("viewWillAppear")
    
    super.viewWillAppear(animated)
  }
  
  override func viewDidAppear(animated: Bool) {
    //println("viewDidAppear")
    
    super.viewDidAppear(animated)
    
    self.updateUIState()
  }
  
  override func viewWillDisappear(animated: Bool) {
    self.displayOnlyMode = false
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - Respond to form's buttons.
  @IBAction func bookButton(sender: AnyObject) {
    //println("bookButton")
    
    if self.booking.rooms.count > 0 {
      let params = [ "guests": self.booking.guests.map({ $0.id }),
        "rooms": self.booking.rooms.map({ $0.id })]
      println(params)
      Alamofire.request(.POST, "http://nodejs-inthehotel.rhcloud.com/bookings/new", parameters: params, encoding: .JSON).responseJSON { (req, res, json, error) in
        if (error != nil) {
          println("error: \(res)")
        } else {
          println("res: \(res)")
          self.booking.parseFromJSON(JSON(json!))
          self.displayOnlyMode = true
          
          self.tableView.reloadData()
          self.updateUIState()
        }
      }
    }
  }
  
  func mergeButton() {
    // Get selected rows and coresponding rooms.
    let selected:[NSIndexPath] = self.tableView.indexPathsForSelectedRows() as [NSIndexPath]
    let rooms:[JTRoom] = selected.map { self.booking.rooms[$0.row] }
    let beds = rooms.reduce(0) { $0+$1.beds }
    
    if beds == 2 || beds == 3 {
      if let newRoom = self.hotel.getAvailableRoom(beds) {
        self.booking.rooms = self.booking.rooms.difference(rooms)
        self.booking.rooms.append(newRoom)
      }
    }
    
    self.tableView.reloadData()
    self.updateUIState()
  }
  
  func addChild() {
    //println("addChild")
    var newGuest = JTGuest()
    
    Alamofire.request(.POST, "http://nodejs-inthehotel.rhcloud.com/guests/new", parameters: ["adult": "false"], encoding: .JSON).responseJSON { (req, res, json, error) in
      if (error != nil) {
        println("error: \(res)")
      } else {
        println("res: \(res)")
        newGuest.parseFromJSON(JSON(json!))
        self.booking.guests.append(newGuest)
        self.updateResults()
      }
    }
  }
  
  func addAdult() {
    //println("addChild")
    var newGuest = JTGuest()
    
    Alamofire.request(.POST, "http://nodejs-inthehotel.rhcloud.com/guests/new", parameters: ["adult": "true"], encoding: .JSON)
      .responseJSON { (req, res, json, error) in
        println("request: \(req)")
        if (error != nil) {
          println("error: \(res)")
        } else {
          println("json: \(json!)")
          newGuest.parseFromJSON(JSON(json!))
          self.booking.guests.append(newGuest)
          self.updateResults()
        }
    }
  }
  
  // MARK: Update UI
  func updateResults() {
    //println("updateResults")
    self.booking.rooms.removeAll(keepCapacity: false)
    Alamofire.request(.GET, "http://nodejs-inthehotel.rhcloud.com/rooms/?available=true").responseJSON { (req, res, json, error) in
      if (error != nil) {
        println("error: \(res)")
      } else {
        let data = JSON(json!)
        for (index:String, value:JSON) in data {
          let newRoom = JTRoom()
          newRoom.parseFromJSON(value)
          self.booking.rooms.append(newRoom)
        }
        self.tableView.reloadData()
        self.updateUIState()
      }
    }
  }
  
  func updateUIState () {
    //println("updateUIState")
    
    if self.displayOnlyMode {
      self.title = "Details"
      self.bookButton.enabled = false
      self.bookButton.title = nil
    } else {
      if let selected = self.tableView.indexPathsForSelectedRows() as? [NSIndexPath] {
        let rooms = selected.map { self.booking.rooms[$0.row] }
        self.title = "\(rooms.reduce(0) { $0 + $1.price }) EUR"
        self.booking.rooms = rooms
        self.bookButton.enabled = true
      } else {
        self.title = "New Booking"
        self.bookButton.enabled = false
      }
    }
  }
  
  func updateFooter(footer: UIView)
  {
    //println("updateHeaderFooter")
    
    if self.displayOnlyMode { return }
    
    switch footer {
    case let row as JTBookingViewRoomFooter:
      if let selected = self.tableView.indexPathsForSelectedRows() as? [NSIndexPath] {
        let rooms = selected.map { self.booking.rooms[$0.row] }
        row.mergeButton.enabled = self.canMergeRooms(rooms)
      } else {
        row.mergeButton.enabled = false
      }
    case let row as JTBookingViewGuestFooter:
      if self.booking.adults == 0 {
        row.childButton.enabled = false
        //println(row.childButton.enabled)
      } else {
        row.childButton.enabled = true
        //println(row.childButton.enabled)
      }
    default:
      break
    }
  }
  
  // MARK: helpers.
  func canMergeRooms(rooms:[JTRoom]) -> Bool {
    //println("canMergeRooms")
    
    if rooms.count == 1 { return false }
    let totalBeds = rooms.reduce(0) {$0 + $1.beds}
    
    if totalBeds == 2 || totalBeds == 3 {
      return Bool(self.hotel.availableRoomWithBeds(totalBeds))
    }
    return false
  }
  
  // MARK: - Table view headers and footers
  
  override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //println("viewForHeaderInSection")
    
    switch (section) {
    case 0:
      let header = tableView.dequeueReusableHeaderFooterViewWithIdentifier("JTBookingViewRoomHeader") as JTBookingViewRoomHeader
      header.leftLabel.text = "Room no."
      header.middleLabel.text = "Beds"
      header.rightLabel.text = "Price"
      return header
    case 1:
      let header = tableView.dequeueReusableHeaderFooterViewWithIdentifier("JTBookingViewGuestHeader") as JTBookingViewGuestHeader
      return header
    default:
      return nil
    }
  }
  
  override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    //println("viewForFooterInSection")
    
    switch (Int(self.displayOnlyMode), section) {
    case (0,0):
      //let footer = tableView.dequeueReusableHeaderFooterViewWithIdentifier("JTBookingViewRoomFooter") as JTBookingViewRoomFooter
      //footer.mergeButton.addTarget(self, action: Selector("mergeButton"), forControlEvents: UIControlEvents.TouchUpInside)
      return nil
    case (0,1):
      let footer = tableView.dequeueReusableHeaderFooterViewWithIdentifier("JTBookingViewGuestFooter") as JTBookingViewGuestFooter
      footer.adultButton.addTarget(self, action: Selector("addAdult"), forControlEvents: UIControlEvents.TouchUpInside)
      footer.childButton.addTarget(self, action: Selector("addChild"), forControlEvents: UIControlEvents.TouchUpInside)
      return footer
    default:
      return nil
    }
  }
  
  override func tableView(tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
    //println("willDisplayFooterView")
    
    self.updateFooter(view)
  }
  
  override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return self.headerHeight
  }
  
  override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return self.headerHeight
  }
  
  // MARK: Displaying sections and cells
  override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    // Return the number of sections.
    return 2
  }
  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    // Return the number of rows in the section.
    if section == 0 { return self.booking.rooms.count }
    if section == 1 { return self.booking.guests.count }
    return 0
  }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    switch (indexPath.section) {
    case 0:
      let cell = tableView.dequeueReusableCellWithIdentifier("JTRoomViewCell", forIndexPath: indexPath) as JTRoomViewCell
      return cell
    case 1:
      let cell = tableView.dequeueReusableCellWithIdentifier("JTGuestViewCell", forIndexPath: indexPath) as JTGuestViewCell
      return cell
    default:
      return UITableViewCell()
    }
  }
  
  override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    switch (indexPath.section) {
    case 0:
      if let currentCell = cell as? JTRoomViewCell {
        currentCell.formatCell(self.booking.rooms[indexPath.row])
      }
    case 1:
      if let currentCell = cell as? JTGuestViewCell {
        currentCell.formatCell(self.booking.guests[indexPath.row])
      }
    default:
      break
    }
  }
  
  // MARK: Selecting cells
  override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
    //println("willSelectRowAtIndexPath")
    
    if indexPath.section == 0  && self.displayOnlyMode == false { return indexPath }
    return nil
  }
  
  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    //println("didSelectRowAtIndexPath")
    
    self.updateUIState()
  }
  
  override func tableView(tableView: UITableView, willDeselectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
    //println("willDeselectRowAtIndexPath")
    
    return indexPath
  }
  
  override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
    //println("didDeselectRowAtIndexPath")
    
    self.updateUIState()
  }
  
  // MARK: Table editing
  // Override to support conditional editing of the table view.
  override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    //println("canEditRowAtIndexPath")
    
    switch (indexPath.section) {
    case 0:
      return false
    case 1:
      return !self.displayOnlyMode
    default:
      return false
    }
  }
  
  // Override to support editing the table view.
  override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    //println("commitEditingStyle")
    
    if indexPath.section == 1 && editingStyle == .Delete {
      let guest = self.booking.guests[indexPath.row]
      Alamofire.request(.DELETE, "http://nodejs-inthehotel.rhcloud.com/guests/id/\(booking.id)").responseJSON { (req, res, json, error) in
        if (error != nil) {
          println("error: \(res)")
        } else {
          println("res: \(res)")
          self.booking.guests.removeAtIndex(indexPath.row)
          tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
          self.updateResults()
        }
      }
    }
  }
  
  
  /*
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
  // Get the new view controller using [segue destinationViewController].
  // Pass the selected object to the new view controller.
  }
  */
  
}
