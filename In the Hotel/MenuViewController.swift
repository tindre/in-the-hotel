//
//  ViewController.swift
//  In the Hotel
//
//  Created by Jakub Tomanik on 26/12/2014.
//  Copyright (c) 2014 Jakub Tomanik. All rights reserved.
//

import UIKit
import MessageUI

class MenuViewController: UIViewController, MFMailComposeViewControllerDelegate {

  @IBOutlet weak var emailMeButton: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.emailMeButton.enabled = MFMailComposeViewController.canSendMail()
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  @IBAction func callMe(sender: AnyObject) {
    UIApplication.sharedApplication().openURL(NSURL(string: "telprompt:+48790416622")!)
  }
  
  @IBAction func mailMe(sender: AnyObject) {
    let composer = MFMailComposeViewController()
    composer.mailComposeDelegate = self
    composer.setSubject("Spotkajmy się :)")
    composer.setToRecipients(["j.tomanik@gmail.com"])
    self.presentViewController(composer, animated: true, completion: nil)
  }

  func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
    controller.dismissViewControllerAnimated(true, completion: nil)
  }
}

