//
//  JTGuestViewCell.swift
//  In the Hotel
//
//  Created by Jakub Tomanik on 26/12/2014.
//  Copyright (c) 2014 Jakub Tomanik. All rights reserved.
//

import UIKit

class JTGuestViewCell: UITableViewCell {
  
    @IBOutlet weak var guestLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
  
  func formatCell(guest: JTGuest) {
    self.guestLabel.text = "Adult"
    if !guest.adult { self.guestLabel.text = "Child" }
  }

}
