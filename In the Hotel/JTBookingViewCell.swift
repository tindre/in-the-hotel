//
//  JTBookingViewCell.swift
//  In the Hotel
//
//  Created by Jakub Tomanik on 26/12/2014.
//  Copyright (c) 2014 Jakub Tomanik. All rights reserved.
//

import UIKit

class JTBookingViewCell: UITableViewCell {
  
  @IBOutlet weak var adultsLabel: UILabel!
  @IBOutlet weak var childrenLabel: UILabel!
  @IBOutlet weak var priceLabel: UILabel!
  @IBOutlet weak var roomsLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
  
  func formatCell(booking: JTBooking) { 
    self.adultsLabel.text = "\(booking.adults)"
    self.childrenLabel.text = "\(booking.children)"
    self.priceLabel.text = "\(booking.totalPrice)"
  }

}
