//
//  JTTableViewController.swift
//  In the Hotel
//
//  Created by Jakub Tomanik on 26/12/2014.
//  Copyright (c) 2014 Jakub Tomanik. All rights reserved.
//

import UIKit
import Alamofire

class JTBookingsViewController: UITableViewController {
  /** reference to main hotel object */
  var table:[JTBooking] = []
  
  override func viewDidLoad() {
    //println("viewDidLoad")
    
    super.viewDidLoad()
    
    self.pullDataFromRemote()
    
    var refreshingControl = UIRefreshControl()
    refreshingControl.addTarget(self, action: Selector("pullDataFromRemote"), forControlEvents: UIControlEvents.ValueChanged)
    self.refreshControl = refreshingControl
  }
  
  func pullDataFromRemote() {
    println("pullDataFromRemote")
    self.table.removeAll(keepCapacity: false)
    Alamofire.request(.GET, "http://nodejs-inthehotel.rhcloud.com/bookings/").responseJSON { (req, res, json, error) in
      if (error != nil) {
        println("error: \(res)")
      } else {
        let data = JSON(json!)
        for (index:String, value:JSON) in data {
          let newBooking = JTBooking()
          newBooking.parseFromJSON(value)
          self.table.append(newBooking)
        }
        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
      }
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - Table view data source
  
  override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    // Return the number of sections.
    return 1
  }
  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    // Return the number of rows in the section.
    return self.table.count
  }
  
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("JTBookingViewCell", forIndexPath: indexPath) as JTBookingViewCell
    return cell
  }
  
  override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    if let currentCell = cell as? JTBookingViewCell {
      currentCell.formatCell(self.table[indexPath.row])
    }
  }
  
  override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    //println("commitEditingStyle")
    
    if editingStyle == UITableViewCellEditingStyle.Delete {
      //println("path: \(indexPath.row), \(indexPath.section)")
      let bookingId = self.table[indexPath.row].id
      
      Alamofire.request(.DELETE, "http://nodejs-inthehotel.rhcloud.com/bookings/id/\(bookingId)").responseJSON { (req, res, json, error) in
        if (error != nil) {
          println("error: \(res)")
        } else {
          self.table.removeAtIndex(indexPath.row)
          tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
      }
    }
  }
  
  
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
    //println("prepareForSegue")
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    //println("segue triggered: \(segue.identifier)")
    if  segue.identifier == "ShowBookingDetails" {
      let bookingViewController = segue.destinationViewController as JTNewBookingViewController
      let index = self.tableView.indexPathForSelectedRow()!.row
      let booking = self.table[index]
      bookingViewController.displayOnlyMode = true
      Alamofire.request(.GET, "http://nodejs-inthehotel.rhcloud.com/bookings/info/\(booking.id)").responseJSON { (req, res, json, error) in
        if (error != nil) {
          println("error: \(res)")
        } else {
          let freshBooking = JTBooking()
          freshBooking.parseFromJSON(JSON(json!))
          bookingViewController.booking = freshBooking
          bookingViewController.tableView.reloadData()
          bookingViewController.updateUIState()
        }
      }
    }
  }
  
  
}
