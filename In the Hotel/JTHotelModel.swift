//
//  JTHotelModel.swift
//  In the Hotel
//
//  Created by Jakub Tomanik on 27/12/2014.
//  Copyright (c) 2014 Jakub Tomanik. All rights reserved.
//

import Alamofire

/**
Class that models the hotel.
*/
class JTHotel {
  
  /**  Array storing objects that reflect rooms in the hotel.  */
  var rooms:[JTRoom] = Array<JTRoom>()
  
  /**  Array storing objects that reflect bookings in the hotel.  */
  var bookings:[JTBooking] = Array<JTBooking>()
  
  /**
  Calculated property, returns the available rooms.
  
  :returns: Array of available rooms.
  */
  var availableRooms:[JTRoom] { return self.rooms.filter { $0.available } }
  
  /**
  Calculated property, returns the number of beds in all available rooms.
  
  :returns: Number of available beds .
  */
  var availableBeds:Int { return self.rooms.reduce(0) {
    $1.available ? $0 + $1.beds : $0 } }
  
  /**
  Calculated property, returns the number of available single rooms.
  
  :returns: Number of available rooms.
  */
  var availableSingleRooms:Int { return self.rooms.reduce(0) {
    $1.available && $1.beds == 1 ? $0 + 1 : $0} }
  
  /**
  Calculated property, returns the number of available double rooms.
  
  :returns: Number of available rooms.
  */
  var availableDoubleRooms:Int { return self.rooms.reduce(0) {
    $1.available && $1.beds == 2 ? $0 + 1 : $0} }
  
  /**
  Calculated property, returns the number of available triple rooms.
  
  :returns: Number of available rooms.
  */
  var availableTripleBedRooms:Int { return self.rooms.reduce(0) {
    $1.available && $1.beds == 3 ? $0 + 1 : $0} }
  
  /**
  Calculated property, returns the maximum number of beds in a room.
  
  :returns: Maximum number of beds in a room.
  */
  var maximumBedsPerRoom:Int { return self.rooms.reduce(0) {
    $1.beds > $0 ? $1.beds : $0} }
  /**
  Finds the number of available rooms which matche the requirements (number of beds).
  
  :param: Number of beds required.
  :returns: Number of available rooms.
  */
  func availableRoomWithBeds(beds:Int) -> Int { return self.rooms.reduce(0) {
    $1.available && $1.beds == beds ? $0 + 1 : $0} }
  
  /**
  Finds first available room which matches the requirements (number of beds).
  
  :param: Number of beds required.
  :returns: Room available. Nil if none is found.
  */
  func getAvailableRoom(beds:Int) -> JTRoom?{
    if 0 > beds || beds > 3 { return nil }
    return self.availableRooms.takeFirst { $0.beds == beds }
  }
  
  /**
  Finds the room by its number.
  
  :param: Number of the room to be found.
  :returns: Room found. Nil if nothing is found.
  */
  func getRoom(number:Int) -> JTRoom? {
    return self.rooms.takeFirst { $0.roomNumber == number }
  }
  
  /**
  Checks rooms' availibity.
  
  :param: Number of the room to be checked.
  :returns: True if the room is available, false if booked.
  */
  func checkRoomAvailabity(number:Int) -> Bool {
    //println("checkRoomAvailabity")
    
    if let room = self.getRoom(number) {
      return room.available
    }
    return false
  }
  
  /**
  Adds new booking and updates the hotel's state.
  
  :param: A booking data.
  :return: True if operation is successful.
  */
  func addBooking(booking: JTBooking) -> Bool {
    //println("addBooking")
    
    /** Checks if the requested rooms are available.
    note: All the added bookings must have a reference to the objects from the hotel instance (rooms).
    However, in the passed booking, the rooms` instances can be created indepedently.
    */
    let normalisedRooms = booking.rooms.mapFilter { room -> JTRoom? in
      if let hotelRoom = self.getRoom(room.roomNumber) {
        if hotelRoom.available == false { return nil }
        hotelRoom.available = false
        hotelRoom.normalisePrice(room)
        return hotelRoom
      }
      return nil
    }
    if normalisedRooms.count != booking.rooms.count { return false }
    booking.rooms = normalisedRooms
    self.bookings.append(booking)
    return true
  }
  
  /**
  Removes booking and updates the hotel's state.
  
  :param: A booking data.
  :return: True if operation is successful.
  */
  func removeBooking(booking: JTBooking) -> Bool {
    //println("removeBooking")
    
    /** Checks if the instance of the booking which we attempt to remove comes from this hotel`s instance.
    */
    var found:Int?
    for (index, value) in enumerate(self.bookings) {
      if self.bookings[index] === booking { found = index }
    }
    if found == nil { return false }
    let dispose = self.bookings.removeAtIndex(found!)
    
    dispose.rooms.each { room in room.checkOut() }
    
    return true
  }
  
  /**
  Loads hotel data from XML model.
  
  :param: XML model of hotel.
  */
  func parseFromXML (element: AnyObject) {
    //println("parseFromXML")
    
    /** format:
    <hotel>
    <rooms>
    <room />
    ...
    </rooms>
    <bookings>
    <booking/>
    ...
    </bookings>
    </hotel>
    if element.tag != "hotel" { return }
    
    let roomsXML = element.child("rooms").children("room")
    
    self.rooms = roomsXML.map({(item:AnyObject) -> JTRoom in
    var newRoom = JTRoom()
    if let element = item as? RXMLElement {
    newRoom.parseFromXML(element)
    }
    return newRoom
    })
    */
  }
  
  /**
  Loads booking data from XML model.
  
  :param: XML model of booking data.
  */
  func loadBookings(element: AnyObject) {
    //println("loadBookings")
    /*
    if element.tag != "bookings" { return }
    
    let bookingsXML = element.children("booking")
    
    self.bookings = bookingsXML.map({(item:AnyObject) -> JTBooking in
    var newBooking = JTBooking()
    if let element = item as? RXMLElement {
    newBooking.parseFromXML(element)
    self.addBooking(newBooking)
    }
    return newBooking
    })
    */
  }
  
  /**
  Returns XML model of instance data.
  
  :return: XML model.
  */
  func parseBookingsToXML () -> String{
    //println("parseBookingsToXML")
    
    var bookingsXML = self.bookings.reduce("") {$0+$1.parseToXML()+""}
    return "<?xml version='1.0' encoding='UTF-8'?>\n<bookings>\n\(bookingsXML)</bookings>\n"
  }
}

/**
Class that models a booking in the hotel.
Adapts Equatable protocol to enable comparison of different instances.
*/
class JTBooking: Equatable {
  var id:String = ""
  /**  An array representing guests in this booking.  */
  var guests:[JTGuest] = Array<JTGuest>()
  
  /**  An array representing rooms for this booking.  */
  var rooms:[JTRoom] = Array<JTRoom>()
  
  /**
  Calculated property, returns the total cost of the booking.
  
  :returns: Total price for the rooms.
  */
  var totalPrice:Int { return self.rooms.reduce(0) {
    $0 + $1.price } }
  
  /**
  Calculated property, returns the number of adults in the booking.
  
  :returns: Total number of adults in the booking.
  */
  var adults:Int { return self.guests.reduce(0) {
    $0 + Int($1.adult) } }
  
  /**
  Calculated property, returns the number of children in the booking.
  
  :returns: Total number of children in the booking.
  */
  var children:Int { return self.guests.reduce(0) {
    $0 + Int(!$1.adult) } }
  
  /**
  Loads data from XML model.
  
  :param: XML model of the booking.
  */
  func parseFromXML (element: AnyObject) {
    /** format:
    <booking>
    <rooms>
    <room />
    ...
    </rooms>
    <guests>
    <guest/>
    ...
    </guests>
    </booking>
    
    if element.tag != "booking" { return }
    
    let roomsXML = element.child("rooms").children("room")
    let guestsXML = element.child("guests").children("guest")
    
    self.rooms = roomsXML.map({(item:AnyObject) -> JTRoom in
    var newRoom = JTRoom()
    if let element = item as? RXMLElement {
    newRoom.parseFromXML(element)
    }
    return newRoom
    })
    
    self.guests = guestsXML.map({(item:AnyObject) -> JTGuest in
    var newGuest = JTGuest()
    if let element = item as? RXMLElement {
    newGuest.parseFromXML(element)
    }
    return newGuest
    })*/
  }
  
  /**
  Returns XML model of the instance's data.
  
  :return: XML model.
  */
  func parseToXML () -> String{
    var guestsXML = self.guests.reduce("") {$0+$1.parseToXML()+"\n"}
    var roomsXML = self.rooms.reduce("") {$0+$1.parseToXML()+"\n"}
    return "<booking>\n<guests>\n\(guestsXML)</guests>\n<rooms>\n\(roomsXML)</rooms>\n</booking>\n"
  }
  
  /**
  Loads data from XML model.
  
  :param: XML model of the room.
  */
  func parseFromJSON (element: JSON) {
    if element["_id"] == nil { return }
    self.id = element["_id"].stringValue
    self.guests.removeAll(keepCapacity: false)
    for (index:String, value:JSON) in element["guests"] {
      let newGuest = JTGuest()
      newGuest.parseFromJSON(value)
      self.guests.append(newGuest)
    }
    self.rooms.removeAll(keepCapacity: false)
    for (index:String, value:JSON) in element["rooms"] {
      let newRoom = JTRoom()
      newRoom.parseFromJSON(value)
      self.rooms.append(newRoom)
    }
  }
  
  /**
  Returns XML model of instance's data.
  
  :return: XML model.
  */
  func parseToJSON () -> JSON{
    var ret:JSON = [
      "_id":self.id,
      "rooms":self.rooms.map({ $0.id }),
      "guests":self.guests.map({ $0.id })
    ]
    return ret
  }
}

/**
Declaration of equality operator for two JTBooking operands.

:param: Left hand side operand.
:param: Right hand side operand.
:returns: True if objects are to be considered equal, false otherwise.
*/
func ==(lhs: JTBooking, rhs: JTBooking) -> Bool {
  if lhs.guests.count != rhs.guests.count { return false }
  if lhs.rooms.count != rhs.rooms.count { return false }
  for (index, room) in enumerate(lhs.rooms) {
    if lhs.rooms[index] != rhs.rooms[index] {return false}
  }
  return true
}

/**
Class that models a guest.
*/
class JTGuest {
  var id:String = ""
  
  /**   Indicates whether the guest is an adult or a child. */
  var adult:Bool = true
  
  /**
  Loads data from XML model.
  
  :param: XML model.
  */
  func parseFromXML (element: AnyObject) {
    /** format:
    <guest type="adult|child"/>
    
    if element.tag != "guest" { return }
    
    let type = element.attribute("type")
    self.adult = (type == "adult")
    */
  }
  
  /**
  Returns XML model of instance's data.
  
  :return: XML model.
  */
  func parseToXML () -> String{
    /** format:
    <guest type="adult|child"/>
    */
    var type = "child"
    if self.adult { type="adult" }
    return "<guest type=\"\(type)\" />"
  }
  
  /**
  Loads data from XML model.
  
  :param: XML model of the room.
  */
  func parseFromJSON (element: JSON) {
    if element["_id"] == nil { return }
    self.id = element["_id"].stringValue
    self.adult = element["adult"].boolValue
  }
  
  /**
  Returns XML model of instance's data.
  
  :return: XML model.
  */
  func parseToJSON () -> JSON{
    var ret:JSON = [
      "_id":self.id,
      "adult":self.adult
    ]
    return ret
  }
}

/**
Class that models a room in the hotel.
Adapts Equatable protocol to enable a comparison of different instances.
*/
class JTRoom: Equatable {
  var id:String = ""
  
  /**  Room number, not necessarily unique */
  var roomNumber:Int = 0
  
  /**  Number of beds in a room. */
  var beds:Int = 0
  
  /**  Price for a room.*/
  var price:Int = 0
  
  /**  Discount given per booking */
  var discount:Int = 0
  
  /**  Indicates whether room is availble for booking. */
  var available:Bool = true
  
  /**
  Default room price may be manually changed for each booking.
  In order to balance the price of the room in terms of different bookings, it`s required to calculate the discount.
  
  :param:referenced room
  */
  func normalisePrice(room: JTRoom) {
    if room == self {
      self.discount = room.price - self.price
      self.price = room.price
    }
  }
  
  /**
  Called when removing room from the booking.
  */
  func checkOut() {
    self.discount = 0
    self.available = true
  }
  
  /**
  Loads data from XML model.
  
  :param: XML model of the room.
  */
  func parseFromXML (element: AnyObject) {
    /** format:
    <room number="n" beds="n" price="n" discount="n"/>
    
    if element.tag != "room" { return }
    self.roomNumber = element.attributeAsInt("number")
    self.beds = element.attributeAsInt("beds")
    self.price = element.attributeAsInt("price")
    self.discount = element.attributeAsInt("discount")
    */
  }
  
  /**
  Returns XML model of instance's data.
  
  :return: XML model.
  */
  func parseToXML () -> String{
    /** format:
    <room number="n" beds="n" price="n" discount="n"/>
    */
    return "<room number=\"\(self.roomNumber)\" beds=\"\(self.beds)\" price=\"\(self.price)\" discount=\"\(self.discount)\" />"
  }
  
  /**
  Loads data from XML model.
  
  :param: XML model of the room.
  */
  func parseFromJSON (element: JSON) {
    if element["_id"] == nil { return }
    self.id = element["_id"].stringValue
    self.roomNumber = element["number"].intValue
    self.beds = element["beds"].intValue
    self.price = element["price"].intValue
    self.discount = element["discount"].intValue
    self.available = element["available"].boolValue
  }
  
  /**
  Returns XML model of instance's data.
  
  :return: XML model.
  */
  func parseToJSON () -> JSON{
    var ret:JSON = [
      "_id":self.id,
      "roomNumber":self.roomNumber,
      "beds":self.beds,
      "price":self.price,
      "discount":self.discount,
      "available":self.available
    ]
    return ret
  }
}

/**
Declaration of equality operator for two JTRoom operands.
Rooms are considered equal if they have the same room number and number of beds.

:param: Left hand side operand.
:param: Right hand side operand.
:returns: True if objects are to be considered equal, false otherwise.
*/
func ==(lhs: JTRoom, rhs: JTRoom) -> Bool {
  return lhs.roomNumber == rhs.roomNumber && lhs.beds == rhs.beds
}