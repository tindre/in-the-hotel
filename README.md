# In the Hotel #

This is my solution to one of the "*Hiring Puzzles*" found on the  [http://www.anixe.pl/careers/](http://www.anixe.pl/careers/)

## Original riddle :##

In a certain hotel, rooms prices are as follows:

* four single (for 1 person) rooms at 50 EUR per night
* two double rooms (for 2 persons) at 60 EUR per night each
* two triple rooms (for 3 persons) at 70 EUR per night each

One day, 3 groups of people come to the hotel:

1. Mother with little Johnny
2. Father, mother, and three mischievous children
3. Four friends returning from vacation


Create an algorithm that accommodates all 3 groups and follows these rules (listed in order of priority):

1. a child cannot sleep without an adult
2. the hotel owner wants to earn as much as possible
3. the groups cannot be mixed in rooms

Write a sample console application that uses the algorithm you create.

## My solution :##
I have risen the bar for myself and instead of writing simple console application I wrote iPhone application that could be a real-life booking app for hotel's reception staff.

Characteristics:

* Fully working iPhone / iPad app.
* Ability to add and delete new bookings. 
* Details about rooms in the hotel are loaded from XML configuration file.
* Bookings are automatically saved to / loaded from local XML file.
* File with bookings can be transferred to a desktop computer using iTunes (via Documents Sharing panel).
* You can get in touch with me from within an app.

## How to use :##
UI was optimised for one hand use on iPhone in portrait position. However it uses auto layout and will work properly on both iPhones and iPads

### Main screen ###
![ItH-main.png](https://bitbucket.org/repo/BypMBB/images/144036228-ItH-main.png)

### New Booking screen ###
![ItH - new booking.png](https://bitbucket.org/repo/BypMBB/images/926177774-ItH%20-%20new%20booking.png)



**From here you can add or delete guests**
![ItH - delete guest.png](https://bitbucket.org/repo/BypMBB/images/4112145860-ItH%20-%20delete%20guest.png)



**Although an algorithm will suggest available rooms, you can manually change them.
If you select rooms and there is abigger room available you can manually "upgrade" selected room. For example if a couple would like to upgrade two single rooms to one double that can be done here.**
![ItH - merge rooms.png](https://bitbucket.org/repo/BypMBB/images/2839773918-ItH%20-%20merge%20rooms.png)



### View Bookings screen ###
Here you can preview and delete all booking in the system.
![ItH - remove booking.png](https://bitbucket.org/repo/BypMBB/images/3718203893-ItH%20-%20remove%20booking.png)



**You can also preview details of any particular booking.**
![ItH - booking details.png](https://bitbucket.org/repo/BypMBB/images/3422930493-ItH%20-%20booking%20details.png)



### View Rooms screen ###
Here you can check status of all rooms in the Hotel.
![ItH - rooms.png](https://bitbucket.org/repo/BypMBB/images/1874084542-ItH%20-%20rooms.png)



## How I can run it? ##
I has been tested on Xcode 6 with iOS8 as a target. Check out this repo and build :)